#include <sourcemod>
#include <cstrike>
#include <zombieplague>

public Plugin: myinfo = {
    name = "[ZP] Addon: Autojoin",
    author = "hadesownage",
    description = "Autojoin players (panorama fix)",
    version = "1.0",
    url = "http://convicts.ro"
}

public void OnPluginStart() {
    HookEvent("player_connect_full", Event_PlayerConnectFull);
}

public void OnMapStart() {

    FindConVar("mp_force_pick_time").IntValue = 0
    FindConVar("mp_limitteams").IntValue = 0

}

public Action Event_PlayerConnectFull(Handle event, const char[] name, bool dontBroadcast) {
    RequestFrame(SetClientTeam, GetEventInt(event, "userid"));
}

public void SetClientTeam(int userid) {
    int client = GetClientOfUserId(userid);

    if (IsPlayerExist(client))
       CS_SwitchTeam(client, determineTeam( ));

}

int determineTeam() {
    int tCount = GetTeamClientCount(CS_TEAM_T);
    int ctCount = GetTeamClientCount(CS_TEAM_CT);
    
    return tCount == ctCount ? GetRandomInt(CS_TEAM_T, CS_TEAM_CT) : tCount < ctCount ? CS_TEAM_T : CS_TEAM_CT;
} 