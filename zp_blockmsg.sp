#include <sourcemod>
#include <cstrike>
#include <sdkhooks>

public Plugin myinfo = {

	name = "[ZP] Addon: Block in-game messages",
	author = "hadesownage",
	description = "Block in-game messages",
	version = "1.0",
	url = "http://www.convicts.ro"

};

public void OnPluginStart( ) {

	HookUserMessage(GetUserMessageId("TextMsg"), Event_TextMsg, true);
	HookUserMessage(GetUserMessageId("SayText2"), SayText2, true);
	HookUserMessage(GetUserMessageId("RadioText"),  RadioMsg, true);
	HookEvent("server_cvar", Event_Cvar, EventHookMode_Pre);
	HookEvent("player_team", Event_PlayerTeam, EventHookMode_Pre);

}

public Action:Event_TextMsg(UserMsg:msg_id, Handle:pb, const players[], playersNum, bool:reliable, bool:init)
{
	if(reliable)
	{
		decl String:text[32];
		PbReadString(pb, "params", text, sizeof(text),0);
		if (StrContains(text, "#Chat_SavePlayer_", false) != -1)
			return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action:SayText2(UserMsg:msg_id, Handle:bf, players[], playersNum, bool:reliable, bool:init)
{
	if(!reliable)
	{
		return Plugin_Continue;
	}

	new String:buffer[25];
	PbReadString(bf, "msg_name", buffer, sizeof(buffer));

	if(StrEqual(buffer, "#Cstrike_Name_Change"))
	{
		return Plugin_Handled;
	}


	return Plugin_Continue;
}

public Action RadioMsg(UserMsg msg_id, Protobuf msg, const int[] players, int playersNum, bool reliable, bool init)
{
	return Plugin_Handled;
}

public Action Event_Cvar(Event event, const char[] name, bool dontBroadcast)
{
	event.BroadcastDisabled = true;
	
	return Plugin_Continue;
}

public Action Event_PlayerTeam(Event event, const char[] name, bool dontBroadcast)
{
	event.SetBool("silent", true); 
	
	return Plugin_Continue;
}
